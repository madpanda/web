let mix = require('laravel-mix');

mix
    .autoload({
        jquery: [ '$', 'window.jQuery', "jQuery", "window.$", "jquery", "window.jquery" ]
    })
    .js('assets/js/index.js', 'public/js')
    .sass('assets/sass/app.scss', 'public/css')
    .options({
        processCssUrls: false
    })
    .sourceMaps(false);

mix.browserSync('madpanda.dev');