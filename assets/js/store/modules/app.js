import * as types from '../mutation-types'

const state = {
  header: {
    shrink: false
  }
}

const mutations = {
  [types.HEADER_SHRINK] (state, config) {
    if (config.hasOwnProperty('shrink')) {
      state.header.shrink = config.shrink
    } else {
      state.header.shrink = true
    }
  }
}

export default {
  state,
  mutations
}