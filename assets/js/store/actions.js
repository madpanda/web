import * as types from './mutation-types'

export const shrinkHeader = ({commit}, config) => {
  if (config instanceof Object) {
    commit(types.HEADER_SHRINK, config)
  }
}