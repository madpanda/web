require('./core/bootstrap')

import Vue from 'vue'
import axios from 'axios'
import NProgress from 'vue-nprogress'
import { sync } from 'vuex-router-sync'

import router from './router'
import store from './store'

import App from './App.vue'

sync(store, router)

Vue.prototype.$http = axios.create()

Vue.use(NProgress, {
  showSpinner: false,
  http: true
})

const nprogress = new NProgress({parent: '.nprogress-container'})

const app = new Vue({
  nprogress,
  router,
  store,
  ...App
})

export { app, store, router }